#include <iostream>

int main()
{
	int n, m, k;
	std::cin >> n >> m >> k;
	if (n * m >= k && (k % n == 0 || k % m == 0)) 
	{
		std::cout << "YES" << std::endl;
	}
	else
	{
		std::cout << "NO" << std::endl;
	}
	return 0;
}