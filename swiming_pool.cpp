#include <iostream>

int main() 
{
	int n, m, x, y, e, w, min1, min2;
	std::cin >> n >> m >> x >> y;
	if (n > m) { std::swap(n,m); }
	e = n - x;
	w = m - y;
	if (x < y) 		 { min1 = x; }
	else	   		 { min1 = y; }
	if (e < w) 		 { min2 = e; }
	else 	   		 { min2 = w; }
	if (min1 > min2) { std::cout << min2 << std::endl; }
	else 			 { std::cout << min1 << std::endl; }
	return 0;
}